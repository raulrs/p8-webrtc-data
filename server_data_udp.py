import socket
import json

SERVER_ADDRESS = ('127.0.0.1', 12345)

def register():
    print("Registering server...")
    udp_socket.sendto("REGISTER SERVER".encode(), SERVER_ADDRESS)

def send_sdp(sdp_message):
    print(f"Sending SDP answer: {sdp_message}")  # Debug message
    udp_socket.sendto(json.dumps(sdp_message).encode(), SERVER_ADDRESS)

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 0))  # Bind to an available port

register()

# Simulated SDP message for demonstration
sdp_answer = {
    'type': 'answer',
    'sdp': 'v=0...'
}
send_sdp(sdp_answer)

# Listening for SDP offers from the client
while True:
    message, address = udp_socket.recvfrom(4096)
    message = message.decode()
    print(f"Received message from {address}: {message}")



