import socket
import json
import time

SERVER_ADDRESS = ('127.0.0.1', 12345)
MAX_MESSAGES = 12

def register():
    print("Registering client...")
    udp_socket.sendto(b"REGISTER CLIENT", SERVER_ADDRESS)

def send_sdp(sdp_message):
    print(f"Sending SDP offer: {sdp_message}")
    udp_socket.sendto(json.dumps(sdp_message).encode(), SERVER_ADDRESS)

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 0))

register()

sdp_offer = {
    'type': 'offer',
    'sdp': 'v=0...'
}

# Send SDP messages
for _ in range(MAX_MESSAGES):
    send_sdp(sdp_offer)
    time.sleep(0.5)

# Send BYE message
udp_socket.sendto('{"type": "bye"}'.encode(), SERVER_ADDRESS)
print("Sent BYE message to server.")

udp_socket.close()
