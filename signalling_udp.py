import socket
import json

clients = {}
servers = {}

def handle_register(message, address):
    if message == "REGISTER CLIENT":
        clients[address] = address
        print(f"Registered client: {address}")
    elif message == "REGISTER SERVER":
        servers[address] = address
        print(f"Registered server: {address}")

def handle_sdp(message, address):
    print(f"Handling SDP from {address}: {message}")  # Debug message
    data = json.loads(message)
    if data['type'] == 'offer' and address in clients:
        for server_address in servers.values():
            udp_socket.sendto(message.encode(), server_address)
            print(f"Forwarded offer from {address} to {server_address}")
    elif data['type'] == 'answer' and address in servers:
        for client_address in clients.values():
            udp_socket.sendto(message.encode(), client_address)
            print(f"Forwarded answer from {address} to {client_address}")

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 12345))

print("Signalling server listening on port 12345")

while True:
    message, address = udp_socket.recvfrom(4096)
    message = message.decode()
    print(f"Received message from {address}: {message}")  # Debug message

    if message.startswith("REGISTER"):
        handle_register(message, address)
    else:
        handle_sdp(message, address)