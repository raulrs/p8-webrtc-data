import socket
import json

SERVER_ADDRESS = ('127.0.0.1', 12345)

def register():
    print("Registering client...")
    udp_socket.sendto("REGISTER CLIENT".encode(), SERVER_ADDRESS)

def send_sdp(sdp_message):
    print(f"Sending SDP offer: {sdp_message}")  # Debug message
    udp_socket.sendto(json.dumps(sdp_message).encode(), SERVER_ADDRESS)

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 0))  # Bind to an available port

register()

# Simulated SDP message for demonstration
sdp_offer = {
    'type': 'offer',
    'sdp': 'v=0...'
}
send_sdp(sdp_offer)

# Listening for SDP answers from the server
while True:
    message, address = udp_socket.recvfrom(4096)
    message = message.decode()
    print(f"Received message from {address}: {message}")










