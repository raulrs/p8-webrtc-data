import socket
import json
import threading

clients = {}
servers = {}

def handle_register(message, address):
    if message == b"REGISTER CLIENT":
        clients[address] = address
        print(f"Registered client: {address}")
    elif message == b"REGISTER SERVER":
        servers[address] = address
        print(f"Registered server: {address}")

def handle_sdp(message, address):
    print(f"Handling SDP from {address}: {message}")  # Debug message
    if message.strip() == '{"type": "bye"}':
        print(f"Received BYE message from {address}")
        if address in clients:
            del clients[address]
        elif address in servers:
            del servers[address]
    else:
        data = json.loads(message)
        if data['type'] == 'offer' and address in clients:
            for server_address in servers.values():
                udp_socket.sendto(message.encode(), server_address)
                print(f"Forwarded offer from {address} to {server_address}")
        elif data['type'] == 'answer' and address in servers:
            for client_address in clients.values():
                udp_socket.sendto(message.encode(), client_address)
                print(f"Forwarded answer from {address} to {client_address}")


def handle_client(connection, address):
    while True:
        message = connection.recv(4096)
        if not message:
            break
        message = message.decode()
        print(f"Received message from {address}: {message}")

        if message.startswith("REGISTER"):
            handle_register(message.encode(), address)
        else:
            close_server = handle_sdp(message, address)
            if close_server:
                udp_socket.close()
                print("No clients or servers left. Closing the server.")
                break

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 12345))

print("Signalling server listening on port 12345")

while True:
    message, address = udp_socket.recvfrom(4096)
    print(f"Received message from {address}: {message}")

    if message.startswith(b"REGISTER"):
        handle_register(message, address)
    else:
        threading.Thread(target=handle_client, args=(udp_socket, address)).start()


