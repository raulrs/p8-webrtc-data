import socket
import json
import time  # Importamos la librería time para introducir un pequeño retraso

SERVER_ADDRESS = ('127.0.0.1', 12345)
MAX_MESSAGES = 12  # Cambiamos a 12 mensajes como especifica el enunciado

def register():
    print("Registering server...")
    udp_socket.sendto(b"REGISTER SERVER", SERVER_ADDRESS)  # Cambiamos a bytes

def send_sdp(sdp_message):
    print(f"Sending SDP answer: {sdp_message}")  # Debug message
    udp_socket.sendto(json.dumps(sdp_message).encode(), SERVER_ADDRESS)

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 0))  # Bind to an available port

register()

# Simulated SDP message for demonstration
sdp_answer = {
    'type': 'answer',
    'sdp': 'v=0...'
}

# Send SDP messages
for _ in range(MAX_MESSAGES):
    send_sdp(sdp_answer)
    time.sleep(0.5)  # Añadimos un pequeño retraso para simular el envío en paralelo

# Send BYE message
udp_socket.sendto('{"type": "bye"}'.encode(), SERVER_ADDRESS)
print("Sent BYE message to server.")

udp_socket.close()

