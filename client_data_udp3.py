import socket
import json

SERVER_ADDRESS = ('127.0.0.1', 12345)
MAX_MESSAGES = 3

def register():
    print("Registering client...")
    udp_socket.sendto("REGISTER CLIENT".encode(), SERVER_ADDRESS)

def send_sdp(sdp_message):
    print(f"Sending SDP offer: {sdp_message}")  # Debug message
    udp_socket.sendto(json.dumps(sdp_message).encode(), SERVER_ADDRESS)

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind(('0.0.0.0', 0))  # Bind to an available port

register()

# Simulated SDP message for demonstration
sdp_offer = {
    'type': 'offer',
    'sdp': 'v=0...'
}
send_sdp(sdp_offer)

messages_sent = 0
messages_received = 0

# Listening for SDP answers from the server
while messages_sent < MAX_MESSAGES and messages_received < MAX_MESSAGES:
    message, address = udp_socket.recvfrom(4096)
    message = message.decode()
    print(f"Received message from {address}: {message}")
    messages_received += 1

    if messages_received == MAX_MESSAGES:
        udp_socket.sendto('{"type": "bye"}'.encode(), SERVER_ADDRESS)
        print("Enviado: {'type': 'bye'}")
        break  # Salir del bucle después de enviar el mensaje BYE

    if message == "BYE":
        print("El servidor ha finalizado la comunicación.")
        break

    # Simulated SDP message for demonstration
    sdp_offer = {
        'type': 'offer',
        'sdp': 'v=0...'
    }
    send_sdp(sdp_offer)

udp_socket.close()
